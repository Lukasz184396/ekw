package ekw;

public enum BeginOfLocalPathForTargetDocFileSave {

    MyLocalPath( "C:\\Users\\L\\Downloads\\" ),
    GeoDrog( "C:\\Users\\Lenovo\\Downloads\\" ),
    GeoDisto( "C:\\Users\\Lukasz\\Downloads\\" ),
    GeoDisto2( "C:\\Documents and Settings\\geo-west\\Moje dokumenty\\Pobrane\\" ),
    GeoDisto3( "C:\\Users\\DELL\\Downloads\\" ),
    GeoDisto4( "C:\\Users\\user\\Downloads\\" );

    private String path;

    BeginOfLocalPathForTargetDocFileSave(String path) {
        this.path = path;
    }

    public String getPath () {
        return path;
    }

}
