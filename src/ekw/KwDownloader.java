package ekw;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class KwDownloader extends JFrame {
	/**
	 * Changed encoding on UTF-8
	 */
	private Robot robot;
	private JButton button;
	private JTextArea jTextArea;
	private static final long serialVersionUID = 1L;
	static Logger logger  = Logger.getLogger("ekw");


	public void setFocusInTextArea() {
		jTextArea.requestFocus();
	}

	public KwDownloader() {
		createGUI();
	}
	
	private void createGUI() {
		setSize(350, 280);
		setTitle("KW DOWNLOADER");
		setLayout(null);
		button = new JButton("Uruchom");
		jTextArea = new JTextArea();
		JScrollPane scroll = new JScrollPane(jTextArea);
		JLabel nrKWLabel = new JLabel("Wpisz numer księgi wieczystej");
		JLabel infoLabel = new JLabel("WZÓR: LD1M_00002233_0_");

		button.addActionListener(new ButtonUruchomListener());

        button.setBounds(20, 160, 300, 50);
        add(button);
        nrKWLabel.setBounds(75, 10, 200, 50);
        add(nrKWLabel);
		infoLabel.setBounds(85, 40, 200, 50);
		add(infoLabel);
        scroll.setBounds(100, 100, 135, 30);
        add(scroll);

	}
	

	private class ButtonUruchomListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			String s[] = jTextArea.getText().split("\\r?\\n");
			try {
				for (int i = 0; i < s.length; i++) {
					logger.setLevel(Level.ALL);
					FileHandler fileHandler = new FileHandler("%h/ekwLog_%g.log", 10000, 10);
					fileHandler.setFormatter(new SimpleFormatter());
					logger.addHandler(fileHandler);
					logger.log(Level.INFO, "Start logging");
					System.setProperty("webdriver.chrome.driver", "C:\\eclipse\\chromedriver.exe");

					String kodW = s[i].substring(0, 4);
					String numerK = s[i].substring(5, 13);
					String cyfraK = s[i].substring(14, 15);

					XWPFDocument document = new XWPFDocument();
					XWPFParagraph tmpParagraph = document.createParagraph();
					XWPFRun tmpRun[] = new XWPFRun[7];
					for (int n = 0; n < 7; n++) {
						tmpRun[n] = tmpParagraph.createRun();
						tmpRun[n].setFontSize(10);
					}

					String pathForGenerateTargetFile = BeginOfLocalPathForTargetDocFileSave.MyLocalPath.getPath() + s[i] + "BADANIE.doc";

					String nagluwekBadania = "                                                            BADANIE KSIĘGI WIECZYSTEJ";
					// word api variable

					tmpRun[0].setText(nagluwekBadania + "\n"
							+ "                                                                          " + kodW + "/"
							+ numerK + "/" + cyfraK + "\n\n"
							+ "                                                                                  DZIAŁ I"
							+ "\n\n");
					tmpRun[0].setBold(true);

					WebDriver driver = new ChromeDriver();
					driver.manage().window().maximize();
					driver.get(
							"https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW?komunikaty=true&kontakt=true&okienkoSerwisowe=false");

					robot = new Robot();

					logger.log(Level.INFO, "Before filling the number of KW on the site");
                    fillNumberOfKWOnWebsite(kodW, numerK, cyfraK, driver);
					logger.log(Level.INFO, "After filling the number of KW on the site");
					//to get time to complete captcha task
					while (!driver.getCurrentUrl().contentEquals(
							"https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW")) {
						Thread.sleep(300);
					}
					

					// rozpoczecie pomiaru czasu
					long start = System.currentTimeMillis();
					Thread.sleep(300);
					KwDownloader.waitForLoad(driver);
					Thread.sleep(300);
					String text = s[i] + "Wydr";
					StringSelection stringSelection = new StringSelection(text);
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

					driver.findElement(By.id("przyciskWydrukDotychczasowy")).click();
					Thread.sleep(300);
					KwDownloader.waitForLoad(driver);
					Thread.sleep(300);

					// **********************************************

					try {
						// getting elements of I section to variables
						String dataBadaniaDzialuI = driver.findElement(By.tagName("h2")).getText().substring(56, 66);
						// move to section I-O
						// ****************************
						driver.findElement(By.xpath("//input[@value='Dział I-O']")).click();
						Thread.sleep(300);
						KwDownloader.waitForLoad(driver);
						Thread.sleep(300);

						String tabXpath[] = new String[2000];
						
						LocationOfTheProperty locationOfTheProperty = new LocationOfTheProperty();

						try {
							int j = 0;
								int zm = 5 + j * 6; // /html/body/div/table[4]/tbody/tr[5]/td[4]
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[4]";
								int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								boolean condition = true;
								while (condition) {

									zm = 5 + j * 6; /// html/body/div/table[4]/tbody/tr[5]/td[4]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[4]";
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.orderNumber[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.orderNumber[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText("POŁOŻENIE NR " + locationOfTheProperty.orderNumber[i] + ", ");
										}
									}

									zm = 6 + j * 6;/// html/body/div/table[4]/tbody/tr[6]/td[3]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.voivodeship[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.voivodeship[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(locationOfTheProperty.voivodeship[i] + ", ");
										}
									}

									zm = 7 + j * 6;//// html/body/div/table[4]/tbody/tr[7]/td[3]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.county[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.county[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(locationOfTheProperty.county[i] + ", ");
										}
									}

									zm = 8 + j * 6;/// html/body/div/table[4]/tbody/tr[8]/td[3]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();// /
									// /
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.borough[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.borough[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(locationOfTheProperty.borough[i] + ", ");
										}
									}

									zm = 9 + j * 6;/// html/body/div/table[4]/tbody/tr[9]/td[3]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.locality[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.locality[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(locationOfTheProperty.locality[i] + ", ");
										}
									}

									zm = 10 + j * 6;/// html/body/div/table[4]/tbody/tr[10]/td[3]
									tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
									value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										locationOfTheProperty.district[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (locationOfTheProperty.district[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(locationOfTheProperty.district[i] + "; \n");
										}
									}

									condition = sizer == 1 && value != "---" && !value.contentEquals("---");
									j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}
						tmpRun[1].setText("\n\n");

						DesignationOfRealEstate designationOfRealEstate = new DesignationOfRealEstate();

						try {
							int j = 0;
							int zm = 8 + j * 15; // /html/body/div/table[5]/tbody/tr[7]/td[4]
							tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {

								zm = 7 + j * 15; // /html/body/div/table[5]/tbody/tr[7]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.parcelId[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.parcelId[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.parcelId[i] + ", ");
									}
								}
								/// html/body/div/table[5]/tbody/tr[8]/td[3]
								zm = 8 + j * 15; //
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.numberOfParcel[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.numberOfParcel[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText("DZ. " + designationOfRealEstate.numberOfParcel[i] + ", ");
									}
								}

								zm = 9 + j * 15; // /html/body/div/table[5]/tbody/tr[9]/td[3]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.numberOfCadastralDistrict[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.numberOfCadastralDistrict[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.numberOfCadastralDistrict[i] + ", ");
									}
								}

								zm = 10 + j * 15; // /html/body/div/table[5]/tbody/tr[10]/td[3]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.nameOfCadastralDistrict[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.nameOfCadastralDistrict[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.nameOfCadastralDistrict[i] + ", ");
									}
								}

								zm = 11 + j * 15; /// html/body/div/table[5]/tbody/tr[11]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.location[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.location[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText("POŁ." + designationOfRealEstate.location[i] + ", ");
									}
								}

								zm = 12 + j * 15; /// html/body/div/table[5]/tbody/tr[12]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.street[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.street[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.street[i] + ", ");
									}
								}

								zm = 13 + j * 15; /// html/body/div/table[5]/tbody/tr[13]/td[3]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.wayOfUse[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.wayOfUse[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.wayOfUse[i] + ", ");
									}
								}

								zm = 14 + j * 15; /// html/body/div/table[5]/tbody/tr[14]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.migrationNrKW[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.migrationNrKW[i].contentEquals("/ /")) {
									} else if ((designationOfRealEstate.migrationNrKW[i].contentEquals("//"))) {
									} else {
										tmpRun[1].setText("DO" + designationOfRealEstate.migrationNrKW[i] + ", ");
									}
								}

								zm = 15 + j * 15; // /html/body/div/table[5]/tbody/tr[15]/td[3]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.migrationArea[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.migrationArea[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.migrationArea[i] + ", ");
									}
								}

								zm = 16 + j * 15; /// html/body/div/table[5]/tbody/tr[16]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.incorporationNrKw[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.incorporationNrKw[i].contentEquals("/ /")) {
									} else if ((designationOfRealEstate.incorporationNrKw[i].contentEquals("//"))) {
									} else {
										tmpRun[1].setText("Z " + designationOfRealEstate.incorporationNrKw[i] + ", ");
									}
								}

								zm = 17 + j * 15; // /html/body/div/table[5]/tbody/tr[17]/td[3]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.incorporationArea[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.incorporationArea[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.incorporationArea[i] + ", ");
									}
								}

								zm = 18 + j * 15; // /html/body/div/table[5]/tbody/tr[18]/td[5]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.numberOfLostKW[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.numberOfLostKW[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.numberOfLostKW[i] + " ");
									}
								}

								zm = 19 + j * 15; /// html/body/div/table[5]/tbody/tr[19]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.numberOfDestroyedKW[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.numberOfDestroyedKW[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.numberOfDestroyedKW[i] + " ");
									}
								}

								zm = 20 + j * 15; /// html/body/div/table[5]/tbody/tr[20]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.numberOfOldKW[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (designationOfRealEstate.numberOfOldKW[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.numberOfOldKW[i] + " ");
									}
								}

								zm = 21 + j * 15; /// html/body/div/table[5]/tbody/tr[21]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									designationOfRealEstate.setOfDocuments[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();

									if (designationOfRealEstate.setOfDocuments[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(designationOfRealEstate.setOfDocuments[i] + " ");
									}
								}

								zm = 7 + j * 15; // /html/body/div/table[5]/tbody/tr[7]/td[4]
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								int zm1 = 8 + j * 15; //
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm1 + "]/td[3]";
								int sizer1 = driver.findElements(By.xpath(tabXpath[zm1])).size();
								String value1 = driver.findElements(By.xpath(tabXpath[zm1])).toString();
								if ((sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /")
										|| (sizer1 == 1 && value1 != "---" && !value1.contentEquals("---")
												&& !value1.contentEquals("/  /") && value1 != "/  /"
												&& !value1.contentEquals("/ /") && value != "/ /")) {
									tmpRun[1].setText("\n\n");
								}
								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						Thread.sleep(300);
						// /html/body/div/table[9]/tbody/tr[5]/td[3]
						String sumOfAreasOfRealEstates = driver
								.findElement(By.xpath("/html/body/div/table[9]/tbody/tr[5]/td[3]")).getText();
						tmpRun[1].setText("\nOBSZAR CAŁEJ NIERUCHOMOŚCI: " + sumOfAreasOfRealEstates + "\n\n");

						String commentXpath = "/html/body/div/table[13]/tbody/tr[6]/td[3]";
						int sizerKoment = driver.findElements(By.xpath(commentXpath)).size();
						if (sizerKoment == 1) {
							String commentOfFirstSection = driver
									.findElement(By.xpath("/html/body/div/table[13]/tbody/tr[6]/td[3]")).getText();
							if (commentOfFirstSection.contentEquals("---")) {
							} else {
								tmpRun[1].setText("KOMENTARZ\n\n" + commentOfFirstSection + "\n\n");
							}
						}

						DocumentOfTheFirstSection documentOfTheFirstSection = new DocumentOfTheFirstSection();

						try {
							int j = 0;
							int zm = 8 + j * 12; // /html/body/div/table[14]/tbody/tr[8]/td[5]
							tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {

								zm = 8 + j * 12; // /html/body/div/table[14]/tbody/tr[8]/td[5]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.nameOfDocument[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.nameOfDocument[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(documentOfTheFirstSection.nameOfDocument[i]);
									}
								}

								zm = 9 + j * 12; // /html/body/div/table[14]/tbody/tr[9]/td[4]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.date[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.date[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + documentOfTheFirstSection.date[i]);
									}
								}

								zm = 10 + j * 12; // /html/body/div/table[14]/tbody/tr[10]/td[4]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.nameOfAuthority[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.nameOfAuthority[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + documentOfTheFirstSection.nameOfAuthority[i]);
									}
								}

								zm = 11 + j * 12; // /html/body/div/table[14]/tbody/tr[11]/td[4]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.headqaurtersOfAuthority[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.headqaurtersOfAuthority[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + documentOfTheFirstSection.headqaurtersOfAuthority[i]);
									}
								}
								zm = 8 + j * 12; // /html/body/div/table[14]/tbody/tr[8]/td[5]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[1].setText("; ");
								}

								zm = 13 + j * 12; // /html/body/div/table[14]/tbody/tr[13]/td[6]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[6]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.numberOfPage[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.numberOfPage[i].contentEquals("---")) {
									} else if (documentOfTheFirstSection.numberOfPage[i].contentEquals("ZBD NR 1")) {
										tmpRun[1].setText(documentOfTheFirstSection.nameOfDocument[i]);
									} else if (documentOfTheFirstSection.numberOfPage[i].contentEquals("DOK.3")) {
										tmpRun[1].setText(documentOfTheFirstSection.nameOfDocument[i]);
									} else {
										tmpRun[1].setText("K." + documentOfTheFirstSection.numberOfPage[i]);
									}
								}

								zm = 14 + j * 12; // /html/body/div/table[14]/tbody/tr[14]/td[4]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									documentOfTheFirstSection.numberOfKW[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (documentOfTheFirstSection.numberOfKW[i].contentEquals("/ /")) {
									} else if ((documentOfTheFirstSection.numberOfKW[i].contentEquals("//"))) {
									} else {
										if (documentOfTheFirstSection.numberOfKW[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(", ");
										}
										tmpRun[1].setText(documentOfTheFirstSection.numberOfKW[i] + " ");
									}
								}

								zm = 8 + j * 12; // /html/body/div/table[14]/tbody/tr[8]/td[5]
								tabXpath[zm] = "/html/body/div/table[14]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[1].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						Thread.sleep(300);
						// zapisz Dział
						// I-O********************************************
						saveHTMLFile(robot);
						Thread.sleep(300);
						text = s[i] + "I-0";
						stringSelection = new StringSelection(text);
						clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, stringSelection);

						paste(robot);

						// przejdz do kolejnego dzialu I-Sp
						// ****************************
						Thread.sleep(300);
						driver.findElement(By.xpath("//input[@value='Dział I-Sp']")).click();

						Thread.sleep(300);
						KwDownloader.waitForLoad(driver);
						Thread.sleep(300);

						// 1.11.1SpisPrawZwiązanychZWłasnoscia

						ListOfPropertyRights listOfPropertyRights = new ListOfPropertyRights();

						try {
							int j = 0;
							int zm = 7 + j * 8; // /html/body/div/table[3]/tbody/tr[7]/td[4]
							tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {

								zm = 7 + j * 8; // /html/body/div/table[3]/tbody/tr[7]/td[4]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.rightNumber[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.rightNumber[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(listOfPropertyRights.rightNumber[i]);
									}
								}

								zm = 8 + j * 8; // /html/body/div/table[3]/tbody/tr[8]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.inscription[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.inscription[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + listOfPropertyRights.inscription[i]);
									}
								}

								zm = 9 + j * 8; // /html/body/div/table[3]/tbody/tr[9]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.kindOfRight[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.kindOfRight[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + listOfPropertyRights.kindOfRight[i]);
									}
								}

								zm = 10 + j * 8; // /html/body/div/table[3]/tbody/tr[10]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.contentOfRight[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.contentOfRight[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(", " + listOfPropertyRights.contentOfRight[i]);
									}
								}

								zm = 12 + j * 8; // /html/body/div/table[3]/tbody/tr[12]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.connectedPage[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.connectedPage[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(listOfPropertyRights.connectedPage[i]);
									}
								}

								zm = 11 + j * 8; // /html/body/div/table[3]/tbody/tr[11]/td[5]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.connectedKW[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.connectedKW[i].contentEquals("/ /")) {
									} else if ((listOfPropertyRights.connectedKW[i]
											.contentEquals("//"))) {
									} else {
										if (listOfPropertyRights.connectedKW[i].contentEquals("---")) {
										} else {
											tmpRun[1].setText(", ");
										}
										tmpRun[1].setText(listOfPropertyRights.connectedKW[i] + " ");
									}
								}

								zm = 13 + j * 8; // /html/body/div/table[3]/tbody/tr[13]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.connectedShare[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.connectedShare[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(listOfPropertyRights.connectedShare[i]);
									}
								}

								zm = 14 + j * 8; // /html/body/div/table[3]/tbody/tr[14]/td[4]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									listOfPropertyRights.kindOfChange[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (listOfPropertyRights.kindOfChange[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(listOfPropertyRights.kindOfChange[i]);
									}
								}

								zm = 8 + j * 8; // /html/body/div/table[3]/tbody/tr[8]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[1].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						// 1.11.2PrawoUzytkowaniaWieczystego

						RightOfPerpetualUsufruct rightOfPerpetualUsufruct = new RightOfPerpetualUsufruct();

						try {
							int j = 0;
							int zm = 5 + j * 2; // /html/body/div/table[4]/tbody/tr[5]/td[3]
							tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {

								zm = 5 + j * 2; // /html/body/div/table[4]/tbody/tr[5]/td[3]
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									rightOfPerpetualUsufruct.periodOfUse[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (rightOfPerpetualUsufruct.periodOfUse[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(rightOfPerpetualUsufruct.periodOfUse[i]);
									}
								}

								zm = 6 + j * 2; // /html/body/div/table[4]/tbody/tr[6]/td[3]
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									rightOfPerpetualUsufruct.wayOfUse[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (rightOfPerpetualUsufruct.wayOfUse[i].contentEquals("---")) {
									} else {
										tmpRun[1].setText(
												", " + rightOfPerpetualUsufruct.wayOfUse[i]);
									}
								}

								zm = 5 + j * 2; // /html/body/div/table[4]/tbody/tr[5]/td[3]
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[1].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						// zapisz Dział
						// I-Sp********************************************
						Thread.sleep(300);
						saveHTMLFile(robot);
						Thread.sleep(300);
						text = s[i] + "I-Sp";
						stringSelection = new StringSelection(text);
						clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, stringSelection);
						paste(robot);

						// move to the section II
						// ******************************
						Thread.sleep(300);
						driver.findElement(By.xpath("//input[@value='Dział II']")).click();

						Thread.sleep(300);
						KwDownloader.waitForLoad(driver);
						Thread.sleep(300);

						// formatka dział II
						tmpRun[2].setText(
								"\n" + "                                                                                  DZIAŁ II"
										+ "\n\n");
						tmpRun[2].setBold(true);

						// getting elements of section  II to variables
						ShareOfOwnerShip shareOfOwnerShip = new ShareOfOwnerShip();
						try {// 5
							int j = 0;
							int zm = 7 + j * 3;
							tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[3]/tbody/tr[7]/td[4]
								zm = 7 + j * 3;
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfOwnerShip.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (shareOfOwnerShip.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("UDZIAŁ NR " + shareOfOwnerShip.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[3]/tbody/tr[8]/td[3]
								zm = 8 + j * 3;
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfOwnerShip.valueOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (shareOfOwnerShip.valueOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(shareOfOwnerShip.valueOfShare[i] + " ");
									}
								}
								// /html/body/div/table[3]/tbody/tr[9]/td[3]
								zm = 9 + j * 3;
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfOwnerShip.kindOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (shareOfOwnerShip.kindOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(shareOfOwnerShip.kindOfShare[i] + " ");
									}
								}

								zm = 7 + j * 3;
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}
								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						tmpRun[3].setText("\n");

						PhysicalPerson physicalPerson = new PhysicalPerson();
						// **************************************************************************************************************
						int k = 0;
						try {// 5
							int j = 0;

							int zm = 5 + j * 8 + k;
							tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[7]/tbody/tr[5]/td[5]
								// ************************************************
								// kilka udzialow

								zm = 6 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[1]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1)
									value = driver.findElement(By.xpath(tabXpath[zm])).getText();
								if (sizer == 1 && value.contentEquals("2.")
										|| (sizer == 1) && value.contentEquals("3.")) //
								{
									zm = 5 + j * 8 + k;
									tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[5]";
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1) {
										physicalPerson.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
												.getText();
										if (physicalPerson.secondSurname[i].contentEquals("---")) {
										} else {
											tmpRun[3].setText("UDZIAŁ NR " + physicalPerson.numberOfShare[i] + " ");
										}
									}
									k = k + 1;
									zm = 5 + j * 8 + k;
									tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1) {
										physicalPerson.secondSurname[i] = driver.findElement(By.xpath(tabXpath[zm]))
												.getText();
										if (physicalPerson.numberOfShare[i].contentEquals("---")) {
										} else {
											tmpRun[3].setText("I " + physicalPerson.numberOfShare[i] + " ");
										}
									}

									k = k + 1;
									zm = 5 + j * 8 + k;
									tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
									sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									if (sizer == 1) {
										physicalPerson.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
												.getText();
										if (physicalPerson.numberOfShare[i].contentEquals("---")) {
										} else {
											tmpRun[3].setText("I " + physicalPerson.numberOfShare[i]);
										}
									}
									tmpRun[3].setText(":   ");// ": "
								}

								// ************************************************
								zm = 5 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("UDZIAŁ NR " + physicalPerson.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[7]/tbody/tr[6]/td[3]
								zm = 6 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.firstName[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.firstName[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(physicalPerson.firstName[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[7]/td[3]
								zm = 7 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.secondName[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.secondName[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(physicalPerson.secondName[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[8]/td[3]
								zm = 8 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.fistSurname[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (physicalPerson.fistSurname[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(physicalPerson.fistSurname[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[9]/td[3]
								zm = 9 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.secondSurname[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (physicalPerson.secondSurname[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(physicalPerson.secondSurname[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[10]/td[3]
								zm = 10 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.nameOfFather[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.nameOfFather[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("O. " + physicalPerson.nameOfFather[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[11]/td[3]
								zm = 11 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.nameOfMother[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.nameOfMother[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("M. " + physicalPerson.nameOfMother[i] + " ");
									}
								}
								// /html/body/div/table[7]/tbody/tr[12]/td[3]
								zm = 12 + j * 8 + k;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									physicalPerson.PESEL[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (physicalPerson.PESEL[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("PESEL " + physicalPerson.PESEL[i] + " ");
									}
								}

								zm = 5 + j * 8;
								tabXpath[zm] = "/html/body/div/table[7]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");

								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						LocalGovernmentUnit localGovernmentUnit  =  new LocalGovernmentUnit();
						// **************************************************************************************************************
						try {// 5
							int j = 0;
							int zm = 5 + j * 7;
							tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[5]/tbody/tr[5]/td[5]
								zm = 5 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("UDZIAŁ NR " + localGovernmentUnit.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[5]/tbody/tr[6]/td[3]
								zm = 6 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.name[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.name[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.name[i] + " ");
									}
								}
								// /html/body/div/table[5]/tbody/tr[7]/td[3]
								zm = 7 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.headquarters[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.headquarters[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.headquarters[i] + " ");
									}
								}
								// /html/body/div/table[5]/tbody/tr[8]/td[3]
								zm = 8 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.nationalBusinessRegistryNumber[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.nationalBusinessRegistryNumber[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.nationalBusinessRegistryNumber[i] + " ");
									}
								}
								zm = 9 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.nameOfAuthorized[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.nameOfAuthorized[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.nameOfAuthorized[i] + " ");
									}
								}
								// /html/body/div/table[5]/tbody/tr[10]/td[3]
								zm = 10 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.headquartersOfAuthorized[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.headquartersOfAuthorized[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.headquartersOfAuthorized[i] + " ");
									}
								}
								// /html/body/div/table[5]/tbody/tr[11]/td[3]
								zm = 11 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									localGovernmentUnit.nationalBusinessRegistryNumberOfAuthorized[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (localGovernmentUnit.nationalBusinessRegistryNumberOfAuthorized[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(localGovernmentUnit.nationalBusinessRegistryNumberOfAuthorized[i] + " ");
									}
								}

								zm = 5 + j * 7;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}
								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						StateTreasury stateTreasury = new StateTreasury();
						// **************************************************************************************************************
						try {// 5
							int j = 0;
							int zm = 5 + j * 6;
							tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[4]/tbody/tr[5]/td[5]
								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (stateTreasury.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("UDZIAŁ NR " + stateTreasury.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[4]/tbody/tr[6]/td[3]
								zm = 6 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.name[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (stateTreasury.name[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(stateTreasury.name[i] + " ");
									}
								}
								// /html/body/div/table[4]/tbody/tr[7]/td[3]
								zm = 7 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.headquarters[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (stateTreasury.headquarters[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(stateTreasury.headquarters[i] + " ");
									}
								}
								// /html/body/div/table[4]/tbody/tr[8]/td[3]
								zm = 8 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.nationalBusinessRegistryNumber[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (stateTreasury.nationalBusinessRegistryNumber[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(stateTreasury.nationalBusinessRegistryNumber[i] + " ");
									}
								}
								// /html/body/div/table[4]/tbody/tr[9]/td[3]
								zm = 9 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.roleOfInstitutions[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (stateTreasury.roleOfInstitutions[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(stateTreasury.roleOfInstitutions[i] + " ");
									}
								}
								// /html/body/div/table[4]/tbody/tr[10]/td[3]
								zm = 10 + j * 6;
								tabXpath[zm] = "/html/body/div/table[5]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									stateTreasury.nationalCourtRegister[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (stateTreasury.nationalCourtRegister[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(stateTreasury.nationalCourtRegister[i] + " ");
									}
								}

								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						// ************************************************************************

						OtherPerson otherPerson = new OtherPerson();
						// **************************************************************************************************************
						try {// 5
							int j = 0;
							int zm = 5 + j * 6;/// html/body/div/table[6]/tbody/tr[5]/td[5]
							tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[4]/tbody/tr[5]/td[5]
								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.numberOfShare[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText("UDZIAŁ NR "
												+ otherPerson.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[6]/tbody/tr[6]/td[3]
								zm = 6 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.name[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.name[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(otherPerson.name[i] + " ");
									}
								}
								// /html/body/div/table[6]/tbody/tr[7]/td[3]
								zm = 7 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.headquarters[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.headquarters[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(otherPerson.headquarters[i] + " ");
									}
								}
								// /html/body/div/table[4]/tbody/tr[8]/td[3]
								zm = 8 + j * 6;
								tabXpath[zm] = "/html/body/div/table[4]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.nationalBusinessRegistryNumber[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.nationalBusinessRegistryNumber[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(otherPerson.nationalBusinessRegistryNumber[i] + " ");
									}
								}
								// /html/body/div/table[6]/tbody/tr[8]/td[3]
								zm = 9 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.transitionState[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.transitionState[i]
											.contentEquals("---")) {
									} else {
										tmpRun[3].setText(
												otherPerson.transitionState[i] + " ");
									}
								}
								// /html/body/div/table[6]/tbody/tr[10]/td[3]
								zm = 10 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									otherPerson.nationalCourtRegister[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (otherPerson.nationalCourtRegister[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(otherPerson.nationalCourtRegister[i] + " ");
									}
								}

								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[6]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						// ************************************************************************

						tmpRun[3].setText("\n");
						ShareOfPerpetualUse shareOfPerpetualUse = new ShareOfPerpetualUse();
						try {// 5
							int j = 0;
							int zm = 5 + j * 3;
							tabXpath[zm] = "/html/body/div/table[10]/tbody/tr[" + zm + "]/td[4]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[10]/tbody/tr[5]/td[4]
								zm = 5 + j * 3;
								tabXpath[zm] = "/html/body/div/table[10]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfPerpetualUse.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (shareOfPerpetualUse.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(
												"UŻYTK. WIECZ. UDZIAŁ NR " + shareOfPerpetualUse.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[10]/tbody/tr[6]/td[3]
								zm = 6 + j * 3;
								tabXpath[zm] = "/html/body/div/table[10]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfPerpetualUse.valueOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (shareOfPerpetualUse.valueOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(shareOfPerpetualUse.valueOfShare[i] + " ");
									}
								}
								// /html/body/div/table[10]/tbody/tr[7]/td[3]
								zm = 7 + j * 3;
								tabXpath[zm] = "/html/body/div/table[10]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									shareOfPerpetualUse.kindOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (shareOfPerpetualUse.kindOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(shareOfPerpetualUse.kindOfShare[i] + " ");
									}
								}

								zm = 5 + j * 3;
								tabXpath[zm] = "/html/body/div/table[10]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}

								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						AnotherPerpetualUser anotherPerpetualUser = new AnotherPerpetualUser();
						// **************************************************************************************************************
						try {// 5
							int j = 0;
							int zm = 5 + j * 6;
							tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[5]";
							int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
							String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
							boolean condition = true;
							while (condition) {
								// /html/body/div/table[13]/tbody/tr[5]/td[5]
								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.numberOfShare[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (anotherPerpetualUser.numberOfShare[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(
												"UŻYTK. WIECZ. UDZIAŁ NR " + anotherPerpetualUser.numberOfShare[i] + ":\t");
									}
								}
								// /html/body/div/table[13]/tbody/tr[6]/td[3]
								zm = 6 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.name[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (anotherPerpetualUser.name[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(anotherPerpetualUser.name[i] + " ");
									}
								}
								// /html/body/div/table[13]/tbody/tr[7]/td[3]
								zm = 7 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.headquarters[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (anotherPerpetualUser.headquarters[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(anotherPerpetualUser.headquarters[i] + " ");
									}
								}
								// /html/body/div/table[13]/tbody/tr[8]/td[3]
								zm = 8 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.nationalBusinessRegistryNumber[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (anotherPerpetualUser.nationalBusinessRegistryNumber[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(anotherPerpetualUser.nationalBusinessRegistryNumber[i] + " ");
									}
								}
								// /html/body/div/table[13]/tbody/tr[9]/td[3]
								zm = 9 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.transitionState[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (anotherPerpetualUser.transitionState[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(anotherPerpetualUser.transitionState[i] + " ");
									}
								}
								// /html/body/div/table[13]/tbody/tr[10]/td[3]
								zm = 10 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								if (sizer == 1) {
									anotherPerpetualUser.nationalCourtRegister[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (anotherPerpetualUser.nationalCourtRegister[i].contentEquals("---")) {
									} else {
										tmpRun[3].setText(anotherPerpetualUser.nationalCourtRegister[i] + " ");
									}
								}
								// /html/body/div/table[13]/tbody/tr[5]/td[3]
								zm = 5 + j * 6;
								tabXpath[zm] = "/html/body/div/table[13]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[3].setText("\n");
								}
								condition = sizer == 1 && value != "---" && !value.contentEquals("---");
								j++;
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}

						tmpRun[3].setText("\n\nDATA OSTATNIEGO WPISU: ");

						ArrayList<String> dateOfSectionIIArrayList = new ArrayList<String>();
						String dateOfSctionIIArray[] = new String[200];
						try {
							for (int j = 0; j < 99; j++) {

								int zm = 13 + j * 14; // /html/body/div/table[15]/tbody/tr[13]/td[3]
								tabXpath[zm] = "/html/body/div/table[15]/tbody/tr[" + zm + "]/td[3]";
								int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									dateOfSctionIIArray[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (dateOfSctionIIArray[i].contentEquals("---")) {
									} else {
										if (dateOfSctionIIArray[i] != null) {
											dateOfSectionIIArrayList.add(dateOfSctionIIArray[i]);
										}
									}
								}
							}
						} catch (org.openqa.selenium.NoSuchElementException e2) {
							e2.printStackTrace();
						}

						if (dateOfSectionIIArrayList.size() != 0) {
							String wyswiel = (String) (dateOfSectionIIArrayList.get(dateOfSectionIIArrayList.size() - 1));
							tmpRun[3].setText(wyswiel.substring(0, 10));
						} else {

							try {
								for (int j = 0; j < 99; j++) {

									int zm = 13 + j * 14; // /html/body/div/table[15]/tbody/tr[13]/td[3]
									tabXpath[zm] = "/html/body/div/table[20]/tbody/tr[" + zm + "]/td[3]";
									int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
									String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
									if (sizer == 1 && value != "---" && !value.contentEquals("---")
											&& !value.contentEquals("/  /") && value != "/  /"
											&& !value.contentEquals("/ /") && value != "/ /") {
										dateOfSctionIIArray[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
										if (dateOfSctionIIArray[i].contentEquals("---")) {
										} else {
											if (dateOfSctionIIArray[i] != null) {
												dateOfSectionIIArrayList.add(dateOfSctionIIArray[i]);
											}
										}
									}
								}
							} catch (org.openqa.selenium.NoSuchElementException e2) {
								e2.printStackTrace();
							}

							String wyswiel = (String) (dateOfSectionIIArrayList.get(dateOfSectionIIArrayList.size() - 1));
							tmpRun[3].setText(wyswiel.substring(0, 10));
						}

						// zapisz Dział II
						// ********************************************
						Thread.sleep(300);
						saveHTMLFile(robot);
						Thread.sleep(300);
						text = s[i] + "II";
						stringSelection = new StringSelection(text);
						clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, stringSelection);
						paste(robot);

						// przejdz do kolejnego dzialu III
						// *****************************
						Thread.sleep(300);
						driver.findElement(By.xpath("//input[@value='Dział III']")).click();

						Thread.sleep(300);
						KwDownloader.waitForLoad(driver);
						Thread.sleep(300);

						// formatka dział III
						tmpRun[4].setText(
								"\n\n" + "                                                                                  DZIAŁ III"
										+ "\n\n");
						tmpRun[4].setBold(true);

						WarningFromTheThirdSection warningFromTheThirdSection = new WarningFromTheThirdSection();
						try {
							for (int j = 0; j < 10; j++) {
								//
								int zm = 16 + j * 8; // /html/body/div/table[3]/tbody/tr[16]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								int sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								String value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.kindOfRegistration[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.kindOfRegistration[i].contentEquals("---")
											|| warningFromTheThirdSection.kindOfRegistration[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.kindOfRegistration[i] + "\n");
									}
								}

								zm = 17 + j * 8; // /html/body/div/table[3]/tbody/tr[17]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.contentsOfRegistration[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.contentsOfRegistration[i].contentEquals("---")
											|| warningFromTheThirdSection.contentsOfRegistration[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.contentsOfRegistration[i] + " ");
									}
								}

								zm = 18 + j * 8; // /html/body/div/table[3]/tbody/tr[18]/td[2]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";

								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.objectOfExecution[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (warningFromTheThirdSection.objectOfExecution[i].contentEquals("---")
											|| warningFromTheThirdSection.objectOfExecution[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.objectOfExecution[i] + " ");
									}
								}

								zm = 19 + j * 8; // /html/body/div/table[3]/tbody/tr[19]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.priority[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.priority[i].contentEquals("---")
											|| warningFromTheThirdSection.priority[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.priority[i] + " ");
									}
								}

								zm = 20 + j * 8; // /html/body/div/table[3]/tbody/tr[20]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.priorityRight[i] = driver.findElement(By.xpath(tabXpath[zm]))
											.getText();
									if (warningFromTheThirdSection.priorityRight[i].contentEquals("---")
											|| warningFromTheThirdSection.priorityRight[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.priorityRight[i] + " ");
									}
								}

								zm = 21 + j * 8; // /html/body/div/table[3]/tbody/tr[21]/td[5]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[5]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.numberOfKWOfRealEstateAlsoCharged[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.numberOfKWOfRealEstateAlsoCharged[i].contentEquals("/ /")) {
									} else if ((warningFromTheThirdSection.numberOfKWOfRealEstateAlsoCharged[i].contentEquals("//"))) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.numberOfKWOfRealEstateAlsoCharged[i] + " ");
									}
								}

								zm = 22 + j * 8; // /html/body/div/table[3]/tbody/tr[22]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.realEstateAlsoChargedNumberOfRegistration[i] = driver
											.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.realEstateAlsoChargedNumberOfRegistration[i].contentEquals("---")
											|| warningFromTheThirdSection.realEstateAlsoChargedNumberOfRegistration[i]
													.contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.realEstateAlsoChargedNumberOfRegistration[i] + " ");
									}
								}

								zm = 23 + j * 8; // /html/body/div/table[3]/tbody/tr[23]/td[4]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[4]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									warningFromTheThirdSection.kindOfChange[i] = driver.findElement(By.xpath(tabXpath[zm])).getText();
									if (warningFromTheThirdSection.kindOfChange[i].contentEquals("---")
											|| warningFromTheThirdSection.kindOfChange[i].contentEquals("Treść pola")) {
									} else {
										tmpRun[5].setText(warningFromTheThirdSection.kindOfChange[i] + " ");
									}
								}

								zm = 16 + j * 8; // /html/body/div/table[3]/tbody/tr[16]/td[3]
								tabXpath[zm] = "/html/body/div/table[3]/tbody/tr[" + zm + "]/td[3]";
								sizer = driver.findElements(By.xpath(tabXpath[zm])).size();
								value = driver.findElements(By.xpath(tabXpath[zm])).toString();
								if (sizer == 1 && value != "---" && !value.contentEquals("---")
										&& !value.contentEquals("/  /") && value != "/  /"
										&& !value.contentEquals("/ /") && value != "/ /") {
									tmpRun[5].setText("\n");
								}
							}
						} catch (org.openqa.selenium.NoSuchElementException e1) {
							e1.printStackTrace();
						}
						tmpRun[6].setText("\n\n\n" + "Księgę zbadał dnia:  " + dataBadaniaDzialuI + "\n\n\n");
						tmpRun[6].setText("Podpis: .................……………..\n\n");
						tmpRun[6].setText("Geodeta uprawniony");

						CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
						setDocumentMargins(sectPr);
						Thread.sleep(300);
						document.write(new FileOutputStream(new File(pathForGenerateTargetFile)));
						// zapisz Dział
						// III*********************************************
						Thread.sleep(300);
						saveHTMLFile(robot);
						Thread.sleep(300);
						text = s[i] + "III";
						stringSelection = new StringSelection(text);
						clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						clipboard.setContents(stringSelection, stringSelection);
						paste(robot);
						Thread.sleep(300);
						try {
							driver.quit();
						} catch (Exception e1) {
							e1.printStackTrace();
							logger.log(Level.INFO, "quit exeption");
						}
						robot.keyPress(KeyEvent.VK_ALT);
						robot.keyPress(KeyEvent.VK_F4);
						robot.keyRelease(KeyEvent.VK_ALT);
						robot.keyRelease(KeyEvent.VK_F4);

					} catch (IOException ex) {
						logger.log(Level.INFO, "Nie można zapisać pliku");
						ex.printStackTrace();
					}
					long stop = System.currentTimeMillis();
					logger.log(Level.INFO,"Czas wykonania calej app (w milisekundach): " + (stop - start));
					document.close();
					System.exit(0);
					// **********************************************
				}

			} catch (Exception eX) {
				eX.printStackTrace();
			}
		}
		// ***************************************************************************

	}

    private void fillNumberOfKWOnWebsite(String kodW, String numerK, String cyfraK, WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("kodWydzialuInput")).clear();
        int asci;
        for (int z = 0; z < kodW.length(); z++) {
            asci = kodW.charAt(z);
            robot.keyPress(asci);
            Thread.sleep(100);
        }

        driver.findElement(By.id("numerKsiegiWieczystej")).clear();
        for (int z = 0; z < numerK.length(); z++) {
            robot.delay(50);
            asci = numerK.charAt(z);
            Thread.sleep(50);
            robot.delay(50);
            robot.keyPress(asci);
            robot.delay(50);
            robot.keyRelease(asci);
            robot.delay(50);
        }

        driver.findElement(By.id("cyfraKontrolna")).clear();
        asci = cyfraK.charAt(0);
        robot.keyPress(asci);
        Thread.sleep(100);
    }

    private void setDocumentMargins(CTSectPr sectPr) {
		CTPageMar pageMar = (CTPageMar) sectPr.addNewPgMar();
		pageMar.setLeft(BigInteger.valueOf(1331L));
		pageMar.setTop(BigInteger.valueOf(1000L));
		pageMar.setRight(BigInteger.valueOf(1331L));
		pageMar.setBottom(BigInteger.valueOf(1000L));
	}

	private void paste(Robot robot) throws InterruptedException {
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	private void saveHTMLFile(Robot robot) {
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_S);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_S);
	}

	private static void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

}